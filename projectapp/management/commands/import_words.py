from django.core.management.base import BaseCommand, CommandError
from projectapp.imp import import_words


class Command(BaseCommand):
    help = 'Import words from file'

    def handle(self, *args, **options):
        import_words()
        self.stdout.write('Successfully import file')