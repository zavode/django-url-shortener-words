import re
from models import Word


def import_words():
    """import words from words.txt"""
    page_file = open("words.txt", "r")
    Word.objects.all().delete()
    word_list = page_file.readlines()
    for line in word_list:
        #clean word
        line = clean_word(line)

        #save word
        save_word(line)

        print(line)
    page_file.close()


def clean_word(word):
    """clean any word"""
    word = word.replace("'", "").rstrip().lower()
    word = re.sub(r'[^a-z0-9]', '', word)
    return word


def save_word(word):
    """save new word and check item exists"""
    word_objects = Word.objects.filter(title=word)
    if not word_objects:
        word_object = Word(title=word)
        word_object.save()
    else:
        word_object = word_objects[0]
    return word_object