from django.contrib import admin
from models import ShortUrl, Word
from projectapp.imp import import_words


def import_words_file(modeladmin, request, queryset):
    import_words()

import_words.short_description = "Import words.txt"


class WordAdmin(admin.ModelAdmin):
    actions = [import_words_file]
    search_fields = ('title', )


class ShortUrlAdmin(admin.ModelAdmin):
    list_display = ('url', 'datetime', )

admin.site.register(Word, WordAdmin)
admin.site.register(ShortUrl, ShortUrlAdmin)